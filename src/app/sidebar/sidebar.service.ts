import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  toggled = false;
  _hasBackgroundImage = true;
  menus = [
    {
      title: 'my-ticket-buddy',
      type: 'header'
    },
    {
      title: 'Dashboard',
      icon: 'fa fa-tachometer-alt',
      active: false,
      Router:"/dashboard"
    },
    {
      title: 'Accounts',
      icon: 'fa fa-shopping-cart',
      active: false,
      type: 'dropdown',
      submenus: [
        {
          title: 'Admins',
        },
        {
          title: 'Customers'
        },
        {
          title: 'Guest Customers'
        }
      ]
    },
    {
      title: 'Offer Codes',
      icon: 'far fa-gem',
      active: false,
    },
    {
      title: 'Booking',
      icon: 'fa fa-chart-line',
      active: false,
    },
    {
      title: 'Reports',
      icon: 'fa fa-globe',
      active: false,
    },
    {
      title: 'Settings',
      icon: 'fa fa-globe',
      active: false,
    },
    {
      title: 'My Account',
      icon: 'fa fa-book',
      active: false
    },
    {
      title: 'SMS/Mail',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Backup',
      icon: 'fa fa-folder',
      active: false,
      type: 'simple'
    },
    {
      title: 'Log Out',
      active: false,
      type: 'simple'
    },
  ];
  constructor() { }

  toggle() {
    this.toggled = ! this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}
